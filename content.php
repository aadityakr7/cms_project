<?php require_once("includes/connection.php"); ?> 
<?php require_once("includes/functions.php"); ?> 
<?php
	find_selected_page();


	/*$qry2="select * from subjects where id='$sel_subj' limit 1";
	$rs=$db->query($qry2);
	$rw=$rs->fetch_assoc();
	echo "<pre>";print_r( $rw);echo "</pre>";*/
?>
<?php include("includes/header.php"); ?> 
<table id="structure">
	<tr>
		<td id="navigation">
			<?php navigation($sel_subject,$sel_page); ?>
		<br />
		<a href="new_subject.php">+ Add a new subject</a>

		</td>
		<td id="page">
			<?php if(!is_null($sel_subject)) { //subject selected ?>
				<h2><?php echo $sel_subject['menu_name']; ?></h2>
			<?php } elseif(!is_null($sel_page)) { //page selected ?>
				<h2><?php echo $sel_page['menu_name']; ?></h2>
				<div class="page-content">
					<?php echo $sel_page['content']; ?>
				</div>
			<?php } else { //nothing selected ?>
				<h2>Select a subject or page to edit</h2>
			<?php } ?>
			<br /><br />
			<a href="edit_page.php?page=<?php echo $sel_page['id']; ?>">Edit page</a>
		</td>
	</tr>
</table>

<?php
// Footer
require("footer.php");
?>