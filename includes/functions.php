<?php
	// Thiis file is the place to store all basic functions

	function check_required_fields($required_array) {
		$field_errors=array();
		//$required_array=array("menu_name","position","visible");
		foreach ($required_array as $fieldname) {
			empty($_POST[$fieldname]) ? $field_errors[$fieldname] = $_POST[$fieldname] . ' is a requied field' : nulll;
		}
		return $field_errors;
	}

	function check_max_field_lengths($field_length_array) {
		$field_errors=array();
		//$fields_with_lengths = array("menu_name" => 30);
		foreach ($field_length_array as $fieldname => $maxlength) {
			if (strlen(trim($_POST[$fieldname])) > $maxlength) {
				$field_errors[]=$fieldname;
			}
		}
		return $field_errors;
	}

	function redirect_to($location = NULL) {
		if($location != NULL) {
			header("Location: {$location}");
			exit;
		}
	}
	function confirm_query($result_set){
		if(!$result_set){
			die("Query execution failed".$db->error());
		}
	}

	function get_all_subjects() {	
		global $db;
		$subject_qry="select * from subjects
					 order by position asc";
		$subject_set=$db->query($subject_qry);
		confirm_query($subject_set);
		return $subject_set;
	}

	function get_all_visible_subjects() {	
		global $db;
		$subject_qry="select * from subjects where visible=1
					 order by position asc";
		$subject_set=$db->query($subject_qry);
		confirm_query($subject_set);
		return $subject_set;
	}

	function get_all_pages() {	
		global $db;
		$page_qry="select * from pages
					 order by position asc";
		$page_set=$db->query($page_qry);
		confirm_query($page_set);
		return $page_set;
	}	

	function get_pages_for_subject($subject_id){
		global $db;
		$page_qry="select * from pages 
					where subject_id={$subject_id} 
					order by position asc";
		$page_set=$db->query($page_qry);
		confirm_query($page_set);
		return $page_set;
	}

	function get_visible_pages_for_subject($subject_id){
		global $db;
		$page_qry="select * from pages 
					where subject_id={$subject_id} and visible=1
					order by position asc";
		$page_set=$db->query($page_qry);
		confirm_query($page_set);
		return $page_set;
	}


	function get_subject_by_id($subject_id){
		global $db;
		$query="select * ";
		$query.="from subjects ";
		$query.="where id='$subject_id'";
		$query.=" limit 1";
		$result_set=$db->query($query);
		confirm_query($result_set);
		// If no rows are returned fetc_array() will return false
		if ($subject=$result_set->fetch_array()) {
			return $subject;
		}else{
			return NULL;
		}
	}

	function get_page_by_id($page_id){
		global $db;
		$query="select * ";
		$query.="from pages ";
		$query.="where id='$page_id'";
		$query.=" limit 1";
		$result_set=$db->query($query);
		confirm_query($result_set);
		// If no rows are returned fetc_array() will return false
		if ($page=$result_set->fetch_array()) {
			return $page;
		}else{
			return NULL;
		}
	}

	function get_default_page($subject_id) {
		$page_set = get_visible_pages_for_subject($subject_id);
		if ($first_page = $page_set->fetch_array()) {
			return $first_page;
		}
		else{
			return NULL;
		}
	}

	function find_selected_page() {
		global $sel_subject;
		global $sel_page;
		if(isset($_GET['subj'])){
			$sel_subject=get_subject_by_id($_GET['subj']);
			$sel_page=get_default_page($sel_subject['id']);
		}elseif (isset($_GET['page'])) {
			$sel_subject=NULL;
			$sel_page=get_page_by_id($_GET['page']);
		}else{
			$sel_subject=NULL;
			$sel_page=NULL;
		}
	}



	function navigation($sel_subject,$sel_page) {
		echo "<ul class=\"subjects\">";

		$subject_set=get_all_subjects();

		// Using database data
		while($subject=$subject_set->fetch_array()){
			echo "<li";
			if($subject['id'] == $sel_subject['id']){ echo " class=\"selected\""; }
				echo "><a href=\"edit_subject.php?subj=".urlencode($subject['id'])."\">{$subject['menu_name']}</a></li>";

				$page_set=get_pages_for_subject($subject['id']);

				echo "<ul class='pages'>";
				// Using database data
				while($page=$page_set->fetch_array()){
					echo "<li";
					if($page['id'] == $sel_page['id']){ echo " class=\"selected\""; }
					echo "><a href=\"content.php?page=".urlencode($page['id'])."\">{$page['menu_name']}</a></li>";
				}
				echo "</ul>";
			}
		echo "</ul>";
	}


	function public_navigation($sel_subject,$sel_page) {
		echo "<ul class=\"subjects\">";

		$subject_set=get_all_visible_subjects();

		// Using database data
		while($subject=$subject_set->fetch_array()){
			echo "<li";
			if($subject['id'] == $sel_subject['id']){ echo " class=\"selected\""; }
				echo "><a href=\"index.php?subj=".urlencode($subject['id'])."\">{$subject['menu_name']}</a></li>";

				if($subject['id'] == $sel_subject['id']){
					$page_set=get_visible_pages_for_subject($subject['id']);

					echo "<ul class='pages'>";
					// Using database data
					while($page=$page_set->fetch_array()){
						echo "<li";
						if($page['id'] == $sel_page['id']){ echo " class=\"selected\""; }
						echo "><a href=\"index.php?page=".urlencode($page['id'])."\">{$page['menu_name']}</a></li>";
					}
					echo "</ul>";
				}
			}
		echo "</ul>";
	}



?>