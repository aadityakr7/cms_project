<?php require_once("includes/connection.php"); ?> 
<?php require_once("includes/functions.php"); ?> 

<?php include("includes/header.php"); ?> 
<?php

	if (isset($_POST['submit'])) {
		$errors = [];
		//var_dump($_POST);
		// Form validation
		$required_fields=array("username","user_password");
		foreach ($required_fields as $fieldname) {
			empty($_POST[$fieldname]) && $_POST[$fieldname]!=0 ? $errors[$fieldname] = $_POST[$fieldname] . ' is a requied field' : nulll;
			// if ( !isset($_POST[$fieldname]) || (empty($_POST[$fieldname]) && $_POST[$fieldname] != 0) ) {
			// 	$errors[]= $fieldname;
			// }
		}

		$fields_with_lengths = array("username" => 30,"user_password" => 30);
		foreach ($fields_with_lengths as $fieldname => $maxlength) {
			if (strlen(trim($_POST[$fieldname])) > $maxlength) {
				$errors[]=$fieldname;
			}
		}

		//var_dump($errors);
		if (empty($errors)) {
			// Perfomr update
			$username=$_POST['username'];
			$user_password=$_POST['user_password'];

			$qry1="select * from users where username='$username' and user_password='$user_password' limit 1";
			$result = $db->query($qry1);
			confirm_query($result);
			if ($result->num_rows == 1) {
				// Authentication success
				$found_user=$result->fetch_array();
				redirect_to("staff.php");
			}
			else {
				// Authentication Failed
				$message="Incorrect username/password combination";
			}

		}
	} // end of if (isset($_POST['submit']))

?>
<table id="structure">
	<tr>
		<td id="navigation">
			<a href="staff.php">Return to Menu</a>
		<br />

		</td>
		<td id="page">
			<h2>Login</h2>
			<?php echo "$message"; ?><br />
			<form action="login.php" method="POST">
				<table>
					<tr>
						<td>Username: </td>
						<td><input type="text" name="username" maxlength="30" /></td>
					</tr>
					<tr>
						<td>Password: </td>
						<td><input type="password" name="user_password" maxlength="30" /></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" name="submit" value="Login" /></td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>

<?php
// Footer
require("footer.php");
?>