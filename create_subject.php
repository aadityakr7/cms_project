<?php require_once("includes/connection.php"); ?> 
<?php require_once("includes/functions.php"); ?> 
<?php
	$errors = [];
	// Form validation
	$required_fields=array("menu_name","position","visible");
	foreach ($required_fields as $fieldname) {
		empty($_POST[$fieldname]) && $_POST[$fieldname]!=0 ? $errors[$fieldname] = $_POST[$fieldname] . ' is a requied field' : nulll;
	}

	$fields_with_lengths = array("menu_name" => 30);
	foreach ($fields_with_lengths as $fieldname => $maxlength) {
		if (strlen(trim($_POST[$fieldname])) > $maxlength) {
			$errors[]=$fieldname;
		}
	}


	if(!empty($errors)) {
		redirect_to("new_subject.php");
	}
?>
<?php
	$menu_name = $_POST['menu_name'];
	$position = $_POST['position'];
	$visible = $_POST['visible'];
?>

<?php
	$qry="insert into subjects (menu_name,position,visible) values('$menu_name',$position,$visible)";
	if($rs=$db->query($qry)) {
		// success
		header("Location: content.php");
		exit; 
	} else {
		// display error
		echo "<p>Subject creation failed</p>";
		echo "<p>".$db->error."</p>";
	}
?>

<?php $db->close(); ?>