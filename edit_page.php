<?php require_once("includes/connection.php"); ?> 
<?php require_once("includes/functions.php"); ?> 

<?php
	if (intval($_GET['page']) == 0) {
		redirect_to("content.php");
	}

	if (isset($_POST['submit'])) {
		$errors = [];
		//var_dump($_POST);
		// Form validation
		$required_fields=array("menu_name","position","visible","content");
		foreach ($required_fields as $fieldname) {
			empty($_POST[$fieldname]) && $_POST[$fieldname]!=0 ? $errors[$fieldname] = $_POST[$fieldname] . ' is a requied field' : nulll;
			// if ( !isset($_POST[$fieldname]) || (empty($_POST[$fieldname]) && $_POST[$fieldname] != 0) ) {
			// 	$errors[]= $fieldname;
			// }
		}

		$fields_with_lengths = array("menu_name" => 30);
		foreach ($fields_with_lengths as $fieldname => $maxlength) {
			if (strlen(trim($_POST[$fieldname])) > $maxlength) {
				$errors[]=$fieldname;
			}
		}

		// Retrieving the values
		$id = $_GET['page'];//echo $id;
		$menu_name=$_POST['menu_name'];//echo $menu_name;
		$position=$_POST['position'];//echo $position;
		$visible=$_POST['visible'];//echo $visible;
		$content=$_POST['content'];//echo $content;

		//var_dump($errors);
		if (empty($errors)) {
			// Perfomr update
			$qry1="update pages set menu_name='$menu_name',position=$position,visible=$visible,content='$content' where id=$id";
			$result = $db->query($qry1);
			//var_dump($result);
			if ($result) {
				// Success
				$message="The page was successfully updated";
			} else {
				// Failed
				$message="The page update failed";
				$message.="<br />".$result->error;
			}

		}
	} // end of if (isset($_POST['submit']))
?>

<?php find_selected_page(); ?>

<?php include("includes/header.php"); ?> 
<table id="structure">
	<tr>
		<td id="navigation">
			<?php navigation($sel_subject,$sel_page); ?>
		</td>
		<td id="page">
			<h2>Edit Page: <?php echo $sel_page['menu_name']; ?></h2>
			<?php echo $message; ?>
			<form action="edit_page.php?page=<?php echo urlencode($sel_page['id']); ?>" method="post">
				<p>Page name:
					<input type="text" name="menu_name" value="<?php echo $sel_page['menu_name']; ?>" id="menu_name" />
					<?php
					if(!empty($errors['menu_name'])){
					echo $errors['menu_name'];
					}
					?>
				</p>
				<p>Position:
					<select name="position">
						<?php
							$page_set=get_all_pages();
							$page_count=mysqli_num_rows($page_set);
							// page_count+1 because we are adding a subject
							for($count=1;$count<=$page_count+1;$count++) {
								// code here
								$str_position="";
								$str_position.= "<option value=\"{$count}\"";
								if ($sel_page['position'] == $count) {
									$str_position.=" selected";
								}
								$str_position.=">{$count}</option>";
								echo $str_position;
							}
						?>

					</select>
				</p>
				<p>Visible:
					<input type="radio" name="visible" value="0"<?php
					if ($sel_page['visible'] == 0) { echo " checked"; }
					?> />No
					&nbsp;
					<input type="radio" name="visible" value="1"<?php
					if ($sel_page['visible'] == 1) { echo " checked"; }
					?> />Yes
				</p>
				<p>Content:
					<input type="text" name="content" value="<?php echo $sel_page['content']; ?>" />
				</p>
				<input type="submit" name="submit" value="Edit Page" />
				&nbsp; &nbsp;
				<a href="delete_page.php?page=<?php echo urlencode($sel_page['id']); ?>" onclick="return confirm('Are you sure?');">Delete page</a>
			</form>

			<a href="content.php">Cancel</a>
			<br /><br /><hr />
		</td>
	</tr>
</table>

<?php
// Footer
require("footer.php");
?>