<?php require_once("includes/connection.php"); ?> 
<?php require_once("includes/functions.php"); ?> 

<?php
	if (intval($_GET['subj']) == 0) {
		redirect_to("content.php");
	}

	if (isset($_POST['submit'])) {
		$errors = [];
		//var_dump($_POST);
		// Form validation
		$required_fields=array("menu_name","position","visible");
		foreach ($required_fields as $fieldname) {
			empty($_POST[$fieldname]) && $_POST[$fieldname]!=0 ? $errors[$fieldname] = $_POST[$fieldname] . ' is a requied field' : nulll;
			// if ( !isset($_POST[$fieldname]) || (empty($_POST[$fieldname]) && $_POST[$fieldname] != 0) ) {
			// 	$errors[]= $fieldname;
			// }
		}

		$fields_with_lengths = array("menu_name" => 30);
		foreach ($fields_with_lengths as $fieldname => $maxlength) {
			if (strlen(trim($_POST[$fieldname])) > $maxlength) {
				$errors[]=$fieldname;
			}
		}

		//var_dump($errors);
		if (empty($errors)) {
			// Perfomr update
			$id = $_GET['subj'];
			$menu_name=$_POST['menu_name'];
			$position=$_POST['position'];
			$visible=$_POST['visible'];

			$qry1="update subjects set menu_name='$menu_name',position='$position',visible='$visible' where id='$id'";
			$result = $db->query($qry1);

			if ($result) {
				// Success
				$message="The subject was successfully updated";
			} else {
				// Failed
				$message="The subject update failed";
				$message.="<br />".$result->error;
			}

		}
	} // end of if (isset($_POST['submit']))
?>

<?php find_selected_page(); ?>

<?php include("includes/header.php"); ?> 
<table id="structure">
	<tr>
		<td id="navigation">
			<?php navigation($sel_subject,$sel_page); ?>
		</td>
		<td id="page">
			<h2>Edit Subject: <?php echo $sel_subject['menu_name']; ?></h2>
			<?php echo $message; ?>
			<form action="edit_subject.php?subj=<?php echo urlencode($sel_subject['id']); ?>" method="post">
				<p>Subject name:
					<input type="text" name="menu_name" value="<?php echo $sel_subject['menu_name']; ?>" id="menu_name" />
					<?php
					if(!empty($errors['menu_name'])){
					echo $errors['menu_name'];
					}
					?>
				</p>
				<p>Position:
					<select name="position">
						<?php
							$subject_set=get_all_subjects();
							$subject_count=mysqli_num_rows($subject_set);
							// subject_count+1 because we are adding a subject
							for($count=1;$count<=$subject_count;$count++) {
								// code here
								$str_position="";
								$str_position.= "<option value=\"{$count}\"";
								if ($sel_subject['position'] == $count) {
									$str_position.=" selected";
								}
								$str_position.=">{$count}</option>";
								echo $str_position;
							}
						?>

					</select>
				</p>
				<p>Visible:
					<input type="radio" name="visible" value="0"<?php
					if ($sel_subject['visible'] == 0) { echo " checked"; }
					?> />No
					&nbsp;
					<input type="radio" name="visible" value="1"<?php
					if ($sel_subject['visible'] == 1) { echo " checked"; }
					?> />Yes
				</p>
				<input type="submit" name="submit" value="Edit Subject" />
				&nbsp; &nbsp;
				<a href="delete_subject.php?subj=<?php echo urlencode($sel_subject['id']); ?>" onclick="return confirm('Are you sure?');">Delete subject</a>
			</form>

			<a href="content.php">Cancel</a>
			<br /><br /><hr />
			<h3>Pages in this subject</h3>

			<br />
			<a href="new_page.php?subj=<?php echo urlencode($sel_subject['id']); ?>">+ Add a new page to this subject</a>
		</td>
	</tr>
</table>

<?php
// Footer
require("footer.php");
?>