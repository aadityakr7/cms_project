<?php require_once("includes/connection.php"); ?> 
<?php require_once("includes/functions.php"); ?> 

<?php
	if (intval($_GET['page']) == 0) {
		redirect_to("content.php");
	}

	$id=$_GET['page'];

	if ($page = get_page_by_id($id)) {
		$qry="delete from pages where id={$id} limit 1";
		$result=$db->query($qry);
		if($result == 1){
			redirect_to("content.php");
		}
		else{
			// delete failed
			echo "<p>Page deletion failed</p>";
			echo $result->error;
			echo "<a href=\"content.php\">Return to main page</a>";
		}
	}
	else{
		// subject doesn't exists
		redirect_to("content.php");
	}
?>

<?php $db->close(); ?>