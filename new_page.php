<?php require_once("includes/connection.php"); ?> 
<?php require_once("includes/functions.php"); ?> 
<?php
	find_selected_page();
	//echo $sel_subject['id'];
?>
<?php
	if (isset($_POST['submit'])) {
		$errors = [];
		// Form validation
		$required_fields=array("menu_name","position","visible");
		foreach ($required_fields as $fieldname) {
			empty($_POST[$fieldname]) && $_POST[$fieldname]!=0 ? $errors[$fieldname] = $_POST[$fieldname] . ' is a requied field' : nulll;
		}

		$fields_with_lengths = array("menu_name" => 30);
		foreach ($fields_with_lengths as $fieldname => $maxlength) {
			if (strlen(trim($_POST[$fieldname])) > $maxlength) {
				$errors[]=$fieldname;
			}
		}


		$menu_name = $_POST['menu_name'];echo $menu_name;
		$position = $_POST['position'];echo $position;
		$visible = $_POST['visible'];echo $visible;
		$content = $_POST['content'];echo $content;
		$subject_id = $sel_subject['id'];echo $subject_id;

		if(empty($errors)) {
			$qry="insert into pages (subject_id,menu_name,position,visible,content) values($subject_id,$menu_name,$position,$visible,$content)";
			if($rs=$db->query($qry)) {
				// success
				header("Location: content.php");
				exit; 
			} else {
				// display error
				echo "<p>Page creation failed</p>";
				echo "<p>".$db->error."</p>";
			}

		}

	}
?>


<?php include("includes/header.php"); ?> 
<table id="structure">
	<tr>
		<td id="navigation">
			<?php navigation($sel_subject,$sel_page); ?>
		</td>
		<td id="page">
			<h2>Add Page</h2>
			<form action="new_page.php" method="post">
				<p>Page name:
					<input type="text" name="menu_name" value="" id="menu_name" />
				</p>
				<p>Position:
					<select name="position">
						<?php
							$page_set=get_pages_for_subject($sel_subject['id']);
							$page_count=mysqli_num_rows($page_set);
							// subject_count+1 because we are adding a subject
							for($count=1;$count<=$page_count+1;$count++) {
								// code here
								echo "<option value=\"{$count}\">{$count}</option>";
							}
						?>

					</select>
				</p>
				<p>Visible:
					<input type="radio" name="visible" value="0" />No
					&nbsp;
					<input type="radio" name="visible" value="1" />Yes
				</p>
				<p>Content:
					<input type="text" name="content" />
				</p>
				<input type="submit" name="submit" value="Add Page" />
			</form>

			<br />
			<a href="content.php">Cancel</a>
		</td>
	</tr>
</table>

<?php
// Footer
require("footer.php");
?>